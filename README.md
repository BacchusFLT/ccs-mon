## CCS Monitor

# What is this?

CCS is short for Computerbrains Cracking Service, one of the leading crackers on the C64 plaform in the very early days, and PHS was one of the leading crackers.

For cracking a c64 program, you need relevant tools and the primary one is the machine code monitor.

This is the machine code monitor that PHS did and also used in a binary format. It's also reassembled into TurboAssembler format. Not my own favourite, but that is what my reassembler exports so that's what we get.

# Configuration

The cart is meant to be in a box where you have a switch to turn off the cart, and also a RESET button to forcefully launch the cart.

You can trigger the warm start using a hit on RESTORE

You trigger a cold start using RESET. This start by making a copy of 0000 - 07FF to D000 - D7FF. A very handy way of preserving a section of the memory normally lost in a reset.

# Commands

(Very early stage of documentation)

```
A - Assemble
B
C - Compare
D - Disassemble
Q - Quit
F - Fill
G - Go
H - Hunt
I - View text
L - Load
M 
N 
O 
R 
S - Save
T 
U 
W 
X 
# 
$ 
* 
; 
: 
% 
' 
- 
+ 
, 
E 
@ 
P 
```
